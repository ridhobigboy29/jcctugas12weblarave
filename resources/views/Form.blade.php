<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Halaman Pendaftaran User baru</h1><br>
    <form action="/kirim" method="post">
        @csrf
        <label for="First">First Name</label> <br>
        <input type="text" name="First" id="first"> <br><br>
        <label for="Last">Last Name</label> <br>
        <input type="text" name="Last" id="Last"> <br><br>
        <label for="Gender">Gender</label> <br>
        <input type="radio" name="Gender" id="Female" value="Female">
        <label for="Female">Female</label> <br>
        <input type="radio" name="Gender" id="Male" value="Male">
        <label for="Male">Male</label> <br>
        <input type="radio" name="Gender" id="Other" value="Other">
        <label for="Other">Other</label> <br><br>
        <label for="Alamat">Alamat</label> <br>
        <textarea name="Alamat" id="" cols="30" rows="10"></textarea> <br><br>
        <input type="Submit" value="kirim">
    </form>
</body>
</html>
